@extends('layouts.app')

@section('content')

@include('layouts.nav')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Liệt kê bình luận truyện</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Tên</th>
                          <th scope="col">Bình luận</th>
                          <th scope="col">Truyện</th>
                          <th scope="col">Người bình luận</th>
                          <th scope="col">Tình trạng</th>
                          <th scope="col">Quản lý</th>  
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($comment as $key => $com)
                        <tr>
                          <th scope="row">{{$key}}</th>
                          <td>{{$com->fullname}}</td>
                          <td>{{$com->comment}} </td>
                          <td>{{$com->truyen->tentruyen}}</td>
                          <td>{{$com->publisher->username}}</td>
                          <td>
                              @if($com->status==0)
                                <span class="text text-success">Đang ẩn</span> 
                              @else
                                <span class="text text-danger">Đang hiển thị</span> 
                              @endif

                          </td>
                          <td>
                            <form method="POST" action="{{route('active_comment')}}">
                                @csrf
                                <input type="hidden" name="comment_id" value="{{$com->id}}">    
                            @if($com->status==0)
                            
                            <input type="hidden" name="action" value="duyet">    
                            <button type="submit" class="btn btn-success">Duyệt</button>
                            @else
                            
                            <input type="hidden" name="action" value="duyet">    
                            <input type="hidden" name="action" value="khongduyet">    
                            <button type="submit" class="btn btn-danger">Không duyệt</button>
                            @endif
                             </form>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
