@extends('../layout')

{{-- @section('slide')

  @include('pages.slide')

@endsection --}}

@section('content')

  

<nav aria-label="breadcrumb">

  <ol class="breadcrumb">

    <li class="breadcrumb-item"><a href="{{url('/')}}">Trang chủ</a></li>

    <li class="breadcrumb-item active" aria-current="page">Truyện yêu thích</li>

  </ol>

</nav>

 <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên truyện</th>
                        <th scope="col">Hình ảnh</th>
                        <th scope="col">Tên User</th>
                        <th scope="col">Quản lý</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($favorites as $key => $favo)
                      <tr>
                        <th scope="row">{{$key}}</th>
                        <td><a href="{{url('xem-truyen/'.$favo->slug)}}" target="_blank">{{$favo->title}}</a></td>
                        <td><img class="card-img-top" width="80px" height="150px" src="{{asset('public/uploads/truyen/'.$favo->image)}}" ></td>
                        <td>{{$favo->publisher->username}}</td>
                        <td>
                            <a href="{{route('xoayeuthich',[$favo->id])}}" class="btn btn-danger btn-small">Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

       



    @endsection