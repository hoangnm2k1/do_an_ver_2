<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    public $timestamps = false; //set time to false
    protected $fillable = [
    	'fullname', 'comment', 'status','truyen_id','publisher_id'
    ];
  
 	protected $table = 'comments';

    public function publisher(){
        return $this->belongsTo(Publisher::class);
    }
    public function truyen(){
        return $this->belongsTo(Truyen::class);
    }
}
